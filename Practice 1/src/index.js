const Koa = require('koa')
const app = new Koa()
const bodyParser = require('koa-body')

const index = require('./routes/index')
const users = require('./routes/user')

app.use(bodyParser())
app.use(users.routes())
app.use(index.routes())

app.listen(3000, () => {
    console.log('Server running at http://localhost:3000/')
})
