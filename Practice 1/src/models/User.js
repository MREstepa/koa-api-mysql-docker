const Sequelize = require('sequelize')
const sequelize = require('../../db/databaseHandler')

module.exports = sequelize.define(
    'user',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            notNull : true
        },
        fullname: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        pin: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
)