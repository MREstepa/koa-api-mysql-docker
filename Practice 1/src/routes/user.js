const Router = require('koa-router')
const router = new Router()
const User = require('../models/User')

// get all users
router.get('/api/users', async ctx => {
    await User.findAll()
        .then(users => {
            ctx.body = users
        })
        .catch(err => {
            ctx.body = "error " + err
        })
})

// post a user
router.post('/api/user', async ctx => {
    if (!ctx.request.body.fullname) {
        ctx.body = {
            error: "Bad Data"
        }
    } else {
        await User.create(ctx.request.body)
            .then(data => {
                ctx.body = data
            })
            .catch(err => {
                ctx.body = "error " + err
            })
    }
})

// get a user
router.get('/api/user/:userid', async ctx => {
    await User.findOne({
        where: {
            id: ctx.params.userid
        }
    })
        .then(data => {
            ctx.body = data
        })
        .catch(() => {
            ctx.body = "User Does not Exist"
        })
})

// delete a user
router.delete('/api/user/:userid', async ctx => {
    await User.destroy({
        where: {
            id: ctx.params.userid
        }
    })
        .then( () => {
            ctx.body = {
                message: "Destroyed Successfully"
            }
        })
        .catch( err => {
            ctx.body = "Error " + err
        })
})

// patch a user information
router.patch('/api/user/:userid', async ctx => {
    await User.update(
        { 
            pin: ctx.request.body.pin,
            email: ctx.request.body.email,
            fullname: ctx.request.body.fullname,
        },
        { where: { id: ctx.params.userid } }
    )
        .then(data => {
            ctx.body = {
                message: "Success"
            }
            for (x in ctx.request.body) {
                console.log(x + ' ' + ctx.request.body[x])
            }
        })
        .catch( err => {
            ctx.body = "Error " + err
        })
})


module.exports = router
