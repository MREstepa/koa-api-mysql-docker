const Router = require('koa-router')
const router = new Router()

router.get('/', async ctx => {
    ctx.body = '<p>Hello world</p>'
})

module.exports = router
