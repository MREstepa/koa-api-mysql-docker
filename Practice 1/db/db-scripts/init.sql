CREATE TABLE `exercise`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fullname` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `pin` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
