const Sequelize = require('sequelize')
const sequelize = new Sequelize('exercise','user','password',{
    host: 'localhost',
    dialect: 'mysql',
    operatorAliases: "0"
})

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    }
);

sequelize.sync({ force: true });
console.log("All models were synchronized successfully.");

module.exports = sequelize
