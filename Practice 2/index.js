const Koa = require('koa')
const app = new Koa()
const bodyParser = require('koa-body')

// DB Connection
require('./src/database/connection')

// Require routes
const user = require('./src/routes/user')

app.use(bodyParser())
app.use(user.routes())


// Server
app.listen(3000, () => {
    console.log('Server running at http://localhost:3000')
})

module.exports = app.listen(3001);
