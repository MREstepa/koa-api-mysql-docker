process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')
const User = require('../src/models/User')
chai.should()

chai.use(chaiHttp)

describe("Test User API", () => {

    /* Test GET all users */
    describe("GET /api/users", () => {
        it("Should GET all users", (done) => {
            chai.request(server)
                .get('/api/users')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                })
        })

        it("Should not GET all users (wrong endpoint)", (done) => {
            chai.request(server)
                .get('/api/user')
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                })
        })
    })


    /* Test POST a user */
    describe("POST /api/users", () => {
        it("Should POST a new user", (done) => {
            const user = {
                email: "email@test1.com",
                password: "password1",
                name: "Test 1"
            };
            chai.request(server)
                .post('/api/users')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id');
                    res.body.should.have.property('email').eq('email@test1.com');
                    res.body.should.have.property('password').eq('password1');
                    res.body.should.have.property('name').eq('Test 1');
                    done();
                })
        })
    })


    /* Test GET a specific user by id */
    describe('GET /api/users/:id', () => {
        it('Should GET a user by the given id', (done) => {
            var user = new User({ 
                email: "email@test2.com",
                password: "password2",
                name: "Test 2",
            });

            user.save((err, user) => {
                chai.request(server)
                    .get('/api/users' + user.id)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('id');
                        res.body.should.have.property('email').eq('email@test1.com');
                        res.body.should.have.property('password').eq('password1');
                        res.body.should.have.property('name').eq('Test 1');
                    })
            });

            done();
        })
    })


    /* Test DELETE a specific user by id */
    describe('DELETE /api/users/:id', () => {
        it('Should DELETE a user by the given id', (done) => {
            var user = new User({ 
                email: "email@test3.com",
                password: "password3",
                name: "Test 3",
            });

            user.save((err, user) => {
                chai.request(server)
                    .delete('/api/users' + user.id)
                    .end((err, res) => {
                        res.should.have.status(200);
                    })
            });

            done();
        })
    })

        
    /* Test PATCH a specific user by id */
    describe('PATCH /api/users/:id', () => {
        it('Should PATCH a user by the given id', (done) => {
            var user = new User({ 
                email: "email@test4.com",
                password: "password4",
                name: "Test 4",
            });

            user.save((err, user) => {
                chai.request(server)
                    .patch('/api/users' + user.id)
                    .send({
                        email: "patched@test4.com",
                        password: "patched4",
                        name: "Patched 4",
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('email').eq('patched@test4.com');
                        res.body.should.have.property('password').eq('patched4');
                        res.body.should.have.property('name').eq('Patched 4');
                    })
            });

            done();
        })
    })

})

