const Sequelize = require('sequelize')
const sequelize = require('../database/connection')

module.exports = sequelize.define(
    "user",  // Model/Table name
    {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        email: Sequelize.STRING(50),
        password: Sequelize.STRING(50),
        name: Sequelize.STRING(100)
    },
);
