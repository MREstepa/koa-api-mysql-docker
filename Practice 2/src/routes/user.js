const Router = require('koa-router')
const router = new Router()

const User = require('../models/User')

// Basic Hello World
router.get('/',  async ctx => {
        ctx.body = "Hello World !!!";
});

// GET all users
router.get('/api/users', async ctx => {
    await User.findAll()
        .then(users => {
            ctx.body = users
        })
        .catch( error => {
            ctx.body = "Error " + error
        })
})
    
// GET a single user
router.get('/api/users/:id', async ctx => {
    await User.findOne({
        where: {
            id: ctx.params.id
        }
    })
        .then(user => {
            ctx.body = user
        })
        .catch( error => {
            ctx.body = "Error " + error
        })
})

// POST a user
router.post('/api/users', async ctx => {
    await User.create(ctx.request.body)
    .then(user => {
        ctx.body = user
        ctx.response.status = 201
        })
        .catch( error => {
            ctx.body = "Error " + error
        })
    })

// PATCH a user information
router.patch('/api/users/:id', async ctx => {
    await User.update(
        {
            email: ctx.request.body.email,
            password: ctx.request.body.password,
            name: ctx.request.body.name
        },
        {
            where: { id: ctx.params.id }
        })
            .then(user => {
                ctx.body = "Updated Successfully"
            })
            .catch( error => {
                ctx.body = "Error " + error
            })
})

// DELETE a user
router.delete('/api/users/:id', async ctx => {
    await User.destroy(
        {
            where: { id: ctx.params.id}
        })
            .then(user => {
                ctx.body = "Destroyed Successfully"
            })
            .catch( error => {
                ctx.body = "Error " + error
            })
})


module.exports = router
