const Sequelize = require('sequelize')

const sequelize = new Sequelize(
    "exercise",  // database name
    "user",      // database username
    "password",  // database password
    {
        host: 'localhost',
        dialect: 'mysql',
        operatorAliases: "0",
        logging: false
    },
)

// Check if connection is successfull
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    }
);

// expose to global
module.exports = sequelize
global.exports = sequelize
