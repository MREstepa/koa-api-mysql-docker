'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "users", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        email: Sequelize.STRING(50),
        password: Sequelize.STRING(50),
        name: Sequelize.STRING(100),

        // Needed
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("users")
  }
};
